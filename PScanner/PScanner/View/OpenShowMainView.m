//
//  OpenShowMainView.m
//  QKH
//
//  Created by Paul on 2018/9/10.
//  Copyright © 2018年 QingHu. All rights reserved.
//

#import "OpenShowMainView.h"

@interface OpenShowMainView()<UIWebViewDelegate>

@end

@implementation OpenShowMainView

- (instancetype)initWithEditFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle] loadNibNamed:@"OpenShowMainView" owner:self options:nil][0];
        self.frame = frame;
    }
    return self;
}

- (instancetype)initWithPreViewFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle] loadNibNamed:@"OpenShowMainView" owner:self options:nil][1];
        [self.mainWebView sizeToFit];
        self.mainWebView.delegate = self;
        self.frame = frame;
    }
    return self;
}

- (instancetype)initWithCQRViewFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle] loadNibNamed:@"OpenShowMainView" owner:self options:nil][2];
        [self.mainWebView sizeToFit];
        self.mainWebView.delegate = self;
        self.frame = frame;
    }
    return self;
}

static BOOL isloadSuc = NO;
static BOOL isloadfail = NO;
-(void)webViewDidStartLoad:(UIWebView *)webView {
    [self.closeBtn setTitle:NSLocalizedString(@"页面加载中...", @"") forState:(UIControlStateNormal)];
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if((!isloadSuc) || (!isloadfail)) {
            [weakSelf.closeBtn setTitle:NSLocalizedString(@"页面加载超时，请重新加载", @"") forState:(UIControlStateNormal)];
        }
    });
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *closeBtnImage = @"close";
    if([webView canGoBack]) {
        closeBtnImage = @"back";
    }
    isloadSuc = YES;
    [self.closeBtn setTitle:NSLocalizedString(@"加载完成", @"") forState:(UIControlStateNormal)];
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.5 delay:0 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
                [strongSelf.closeBtn setTitle:@"" forState:(UIControlStateNormal)];
                strongSelf.closeBtn.frame = ({
                    CGRect frame = strongSelf.closeBtn.frame;
                    frame.origin.x = 8;
                    frame.size.width = frame.size.height;
                    frame;
                });
                strongSelf.closeBtn.layer.cornerRadius = strongSelf.closeBtn.frame.size.height/2.0;
            } completion:^(BOOL finished) {
                [strongSelf.closeBtn setTintColor:[UIColor whiteColor]];
                [strongSelf.closeBtn setImage:[UIImage imageNamed:closeBtnImage] forState:(UIControlStateNormal)];
                [UIView animateWithDuration:0.5 animations:^{
                    strongSelf.closeBtn.frame = ({
                        CGRect frame = strongSelf.closeBtn.frame;
                        frame.origin.y = 8;
                        frame;
                    });
                }];
            }];
        });
    });
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    isloadfail = YES;
    [self.closeBtn setTitle:NSLocalizedString(@"加载失败", @"") forState:(UIControlStateNormal)];
}

@end
