//
//  OpenShowMainView.h
//  QKH
//
//  Created by Paul on 2018/9/10.
//  Copyright © 2018年 QingHu. All rights reserved.
//

/**
 * 自定义的弹窗框视图样式，使用OpenShowView承载该视图
 **/

#import <UIKit/UIKit.h>

@interface OpenShowMainView : UIView

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *favoriteBtn;
@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITextField *detailTextField;
@property (strong, nonatomic) IBOutlet UITextField *remarkTextField;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *sureBtn;
@property (strong, nonatomic) IBOutlet UILabel *msgTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *msgDetailLabel;
@property (strong, nonatomic) IBOutlet UILabel *msgRemarkLabel;


@property (strong, nonatomic) IBOutlet UIWebView *mainWebView;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;

@property (strong, nonatomic) IBOutlet UILabel *cqr_titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *cqr_contentTextField;
@property (strong, nonatomic) IBOutlet UIImageView *cqr_imageView;
@property (strong, nonatomic) IBOutlet UIButton *cqr_createBtn;


- (instancetype)initWithEditFrame:(CGRect)frame;
- (instancetype)initWithPreViewFrame:(CGRect)frame;
- (instancetype)initWithCQRViewFrame:(CGRect)frame;

@end
