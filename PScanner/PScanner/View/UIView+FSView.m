//
//  UIView+FSView.m
//  XZSQ
//
//  Created by Paul on 2019/7/5.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import "UIView+FSView.h"

@implementation UIView (FSView)

-(void)removeAllSubviews {
    NSArray *viewsToRemove = [self subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
}

-(CGFloat)top {
    return self.frame.origin.y;
}

-(void)setTop:(CGFloat)top {
    self.frame = ({
        CGRect frame = self.frame;
        frame.origin.y = top;
        frame;
    });
}

-(CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

-(void)setBottom:(CGFloat)bottom {
    self.frame = ({
        CGRect frame = self.frame;
        frame.origin.y = bottom - frame.size.height;
        frame;
    });
}

-(CGFloat)left {
    return self.frame.origin.x;
}

-(void)setLeft:(CGFloat)left {
    self.frame = ({
        CGRect frame = self.frame;
        frame.origin.x = left;
        frame;
    });
}

-(CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}

-(void)setRight:(CGFloat)right {
    self.frame = ({
        CGRect frame = self.frame;
        frame.origin.x = right - frame.size.width;
        frame;
    });
}

-(CGFloat)height {
    return self.frame.size.height;
}

-(void)setHeight:(CGFloat)height {
    self.frame = ({
        CGRect frame = self.frame;
        frame.size.height = height;
        frame;
    });
}

-(CGFloat)width {
    return self.frame.size.width;
}

-(void)setWidth:(CGFloat)width {
    self.frame = ({
        CGRect frame = self.frame;
        frame.size.width = width;
        frame;
    });
}

-(CGFloat)centerX {
    return self.center.x;
}

-(void)setCenterX:(CGFloat)centerX {
    self.center = ({
        CGPoint point = self.center;
        point.x = centerX;
        point;
    });
}

-(CGFloat)centerY {
    return self.center.y;
}

-(void)setCenterY:(CGFloat)centerY {
    self.center = ({
        CGPoint point = self.center;
        point.y = centerY;
        point;
    });
}

-(CGRect)screenFrame {
    return self.window.bounds;
}

-(void)setScreenFrame:(CGRect)screenFrame {
    self.window.frame = screenFrame;
}

-(CGSize)size {
    return self.frame.size;
}

- (void)setSize:(CGSize)size {
    self.frame = ({
        CGRect frame = self.frame;
        frame.size = size;
        frame;
    });
}

@end
