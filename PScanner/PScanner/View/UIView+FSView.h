//
//  UIView+FSView.h
//  XZSQ
//
//  Created by Paul on 2019/7/5.
//  Copyright © 2019 QingHu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define ScreenHeight                                      [UIScreen mainScreen].bounds.size.height
#define ScreenWidth                                       [UIScreen mainScreen].bounds.size.width

@interface UIView (FSView)

@property (nonatomic, assign) CGFloat top;

@property (nonatomic, assign) CGFloat bottom;

@property (nonatomic, assign) CGFloat left;

@property (nonatomic, assign) CGFloat right;

@property (nonatomic, assign) CGFloat height;

@property (nonatomic, assign) CGFloat width;

@property (nonatomic, assign) CGFloat centerX;

@property (nonatomic, assign) CGFloat centerY;

@property (nonatomic, assign) CGRect screenFrame;

@property (nonatomic, assign) CGSize size;

-(void)removeAllSubviews;

@end

NS_ASSUME_NONNULL_END
