//
//  HistoryListTableViewCell.h
//  PScanner
//
//  Created by LiuBo on 2019/7/26.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HistoryListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;
@property (strong, nonatomic) IBOutlet UILabel *remarkLabel;
@property (strong, nonatomic) IBOutlet UIButton *preViewBtn;
@property (strong, nonatomic) IBOutlet UIButton *favoriteBtn;

@end

NS_ASSUME_NONNULL_END
