//
//  ViewController.m
//  PScanner
//
//  Created by LiuBo on 2019/7/25.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "QRViewController.h"
#import "FFDropDownMenuView.h"
#import "HistoryListTableViewController.h"
#import "FSCacheManager.h"
#import "QRSourceHelper.h"
#import "UILabel+AutomaticWriting.h"
#import "AboutUsViewController.h"
#import "PS_GeneralTool.h"
#import "OpenShowMainView.h"
#import "OpenShowView.h"

@interface ViewController ()
<
    QRViewControllerDelegate, 
    FFDropDownMenuViewDelegate, 
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    UIGestureRecognizerDelegate,
    OpenShowViewDelegate
>
@property (nonatomic, strong) UILabel *scanContentLabel;
@property (nonatomic, strong) UIButton *scanBtn;
@property (nonatomic, strong) UIButton *createQRImageBtn;
@property (nonatomic, strong) UIButton *scanLocalQRImageBtn;
@property (nonatomic, copy) NSString *scanContentString;
@property (nonatomic, strong) FFDropDownMenuView *dropDownMenu;
@property (nonatomic, strong) CIDetector *detector;
@property (nonatomic, strong) OpenShowMainView *cqr_mainView;
@property (nonatomic, strong) UIImage *qr_image;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = NSLocalizedString(@"AppName", @"App名字");
    [self addStartAnimate];
    [self setRightItem];
    [self setMenuView];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CanScanPasteboard"];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"CanScanPasteboard"]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CanScanPasteboard"];
        __weak typeof(self) weakSelf = self;
        if([UIPasteboard generalPasteboard].string.length > 0 &&
           (![[UIPasteboard generalPasteboard].string isEqual:self.scanContentString])) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"检测到复制了新内容，是否马上生成二维码？", @"") preferredStyle:(UIAlertControllerStyleAlert)];
            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"马上生成", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                weakSelf.cqr_mainView = [[OpenShowMainView alloc] initWithCQRViewFrame:CGRectMake(30, 120, UIScreen.mainScreen.bounds.size.width - 60, 350)];
                weakSelf.cqr_mainView.clipsToBounds = YES;
                weakSelf.cqr_mainView.layer.cornerRadius = 10;
                
                weakSelf.cqr_mainView.cqr_titleLabel.text = NSLocalizedString(@"生成二维码", @"");
                weakSelf.cqr_mainView.cqr_contentTextField.placeholder = NSLocalizedString(@"请输入要生成的二维码内容", @"");
                [weakSelf.cqr_mainView.cqr_createBtn setTitle:NSLocalizedString(@"立即生成", @"") forState:(UIControlStateNormal)];
                
                weakSelf.cqr_mainView.cqr_contentTextField.text = [UIPasteboard generalPasteboard].string;
                [weakSelf.cqr_mainView.cqr_contentTextField addTarget:weakSelf action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
                [weakSelf.cqr_mainView.cqr_createBtn addTarget:weakSelf action:@selector(cqr_createBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
                weakSelf.cqr_mainView.cqr_createBtn.tag = 2309;
                [[OpenShowView shareInstance] showDeledate:weakSelf showView:weakSelf.cqr_mainView];
                [weakSelf cqr_createBtnAction:weakSelf.cqr_mainView.cqr_createBtn];
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"以后再说", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }

}
-(void)addStartAnimate {
    self.scanContentLabel.center = self.view.center;
    self.scanContentLabel.bounds = CGRectZero;

    self.scanBtn.center = self.view.center;
    self.scanBtn.bounds = CGRectZero;

    self.scanLocalQRImageBtn.center = self.view.center;
    self.scanLocalQRImageBtn.bounds = CGRectZero;
    
    self.createQRImageBtn.center = self.view.center;
    self.createQRImageBtn.bounds = CGRectZero;
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.5 animations:^{
        weakSelf.scanContentLabel.frame = ({
            CGRect frame = weakSelf.scanContentLabel.frame;
            frame.size.height = 40;
            frame.size.width = [UIScreen mainScreen].bounds.size.width - 20;
            frame.origin.x = ([UIScreen mainScreen].bounds.size.width - frame.size.width)/2.0;
            frame.origin.y = ([UIScreen mainScreen].bounds.size.height - frame.size.height)/2.0;
            frame;
        });
        weakSelf.scanContentLabel.layer.cornerRadius = weakSelf.scanContentLabel.frame.size.height/2.0;
        
        weakSelf.scanBtn.frame = ({
            CGRect frame = weakSelf.scanBtn.frame;
            frame.size.height = weakSelf.view.frame.size.width/2.0;
            frame.size.width = weakSelf.view.frame.size.width/2.0;
            frame.origin.x = ([UIScreen mainScreen].bounds.size.width - frame.size.width)/2.0;
            frame.origin.y = ([UIScreen mainScreen].bounds.size.height - frame.size.height)/2.0;
            frame;
        });
        weakSelf.scanBtn.layer.cornerRadius = weakSelf.scanBtn.frame.size.height/2.0;        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.5 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
            weakSelf.scanContentLabel.frame = ({
                CGRect frame = weakSelf.scanContentLabel.frame;
                frame.origin.y = 80 + 64;
                frame;
            });
            
            weakSelf.scanBtn.frame = ({
                CGRect frame = weakSelf.scanBtn.frame;
                frame.origin.y = weakSelf.view.frame.size.height/2.5;
                frame;
            });
            weakSelf.scanLocalQRImageBtn.frame = ({
                CGRect frame = weakSelf.scanLocalQRImageBtn.frame;
                frame.origin.y = weakSelf.scanBtn.frame.origin.y + weakSelf.scanBtn.frame.size.height + 20;
                frame;
            });
            weakSelf.createQRImageBtn.frame = ({
                CGRect frame = weakSelf.createQRImageBtn.frame;
                frame.origin.y = weakSelf.scanBtn.frame.origin.y + weakSelf.scanBtn.frame.size.height + 80;
                frame;
            });
        } completion:^(BOOL finished) {
            [weakSelf refreshScanContentLabelText:NSLocalizedString(@"点击下方按钮", @"")];
            [weakSelf.scanBtn setTitle:NSLocalizedString(@"开始扫描", @"") forState:UIControlStateNormal];
            [UIView animateWithDuration:0.5 animations:^{
                weakSelf.scanLocalQRImageBtn.frame = ({
                    CGRect frame = weakSelf.scanLocalQRImageBtn.frame;
                    frame.origin.x = weakSelf.scanContentLabel.frame.origin.x + 20;
                    frame.size.width = weakSelf.scanContentLabel.frame.size.width - 40;
                    frame.size.height = 40;
                    frame;
                });
                weakSelf.scanLocalQRImageBtn.layer.cornerRadius = weakSelf.scanLocalQRImageBtn.frame.size.height/2.0;
                weakSelf.createQRImageBtn.frame = ({
                    CGRect frame = weakSelf.createQRImageBtn.frame;
                    frame.origin.x = weakSelf.scanContentLabel.frame.origin.x;
                    frame.size.width = weakSelf.scanContentLabel.frame.size.width;
                    frame.size.height = 40;
                    frame;
                });
                weakSelf.createQRImageBtn.layer.cornerRadius = weakSelf.createQRImageBtn.frame.size.height/2.0;
            }];
        }];
    }];

}

-(void)setRightItem {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"more"] style:(UIBarButtonItemStyleDone) target:self action:@selector(moreBtnClick:)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithRed:138/255.0 green:138/255.0 blue:138/255.0 alpha:1.0];
}

-(void)moreBtnClick:(UIButton *)sender {
    [self.dropDownMenu showMenu];
}

//-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    if(self.navigationController.navigationBarHidden) {
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
//    } else {
//        [self.navigationController setNavigationBarHidden:YES animated:YES];
//    }
//    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder)to:nil from:nil forEvent:nil];
//}

-(void)setMenuView {
    NSArray *modelsArray = [self getMenuModelsArray];
    self.dropDownMenu = [FFDropDownMenuView ff_DefaultStyleDropDownMenuWithMenuModelsArray:modelsArray menuWidth:-10 eachItemHeight:-10 menuRightMargin:-10 triangleRightMargin:-10];
}

//创建下拉列表数据源
- (NSArray *)getMenuModelsArray {
    __weak typeof(self) weakSelf = self;
    FFDropDownMenuModel *menuModel0 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:NSLocalizedString(@"扫码记录", @"") menuItemIconName:@"scan"  menuBlock:^{
        HistoryListTableViewController *historyList = [[HistoryListTableViewController alloc] init];
        [weakSelf.navigationController pushViewController:historyList animated:YES];
    }];
    FFDropDownMenuModel *menuModel2 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:NSLocalizedString(@"设置中心", @"") menuItemIconName:@"aboutus" menuBlock:^{
        AboutUsViewController *aboutUs = [[AboutUsViewController alloc] initWithNibName:@"AboutUsViewController" bundle:[NSBundle mainBundle]];
        aboutUs.hidesBottomBarWhenPushed = YES;
        [weakSelf.navigationController pushViewController:aboutUs animated:YES];
    }];
    
    NSArray *menuModelArr = @[menuModel0, menuModel2];
    return menuModelArr;
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{ CIDetectorAccuracy : CIDetectorAccuracyHigh}];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    if (!image) {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    NSArray *features = [self.detector featuresInImage:[CIImage imageWithCGImage:image.CGImage]];
    if (features.count >=1) {
        CIQRCodeFeature *feature = [features objectAtIndex:0];
        NSString *scannedResult = feature.messageString;
        [[QRSourceHelper sharedService] saveScanString:scannedResult];
        [self finishScanWithContent:scannedResult];
    } else {
        [self finishScanWithContent:nil];
    }
}

-(void)scanBtnClickAction:(UIButton *)sender {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        QRViewController *qRvc = [[QRViewController alloc] init];
        qRvc.delegate = weakSelf;
        [weakSelf.navigationController pushViewController:qRvc animated:YES];
    });
}

-(void)createQRImageBtnAction:(UIButton *)sender {
    self.cqr_mainView = [[OpenShowMainView alloc] initWithCQRViewFrame:CGRectMake(30, 120, UIScreen.mainScreen.bounds.size.width - 60, 350)];
    self.cqr_mainView.clipsToBounds = YES;
    self.cqr_mainView.layer.cornerRadius = 10;
    
    self.cqr_mainView.cqr_titleLabel.text = NSLocalizedString(@"生成二维码", @"");
    self.cqr_mainView.cqr_contentTextField.placeholder = NSLocalizedString(@"请输入要生成的二维码内容", @"");
    [self.cqr_mainView.cqr_createBtn setTitle:NSLocalizedString(@"立即生成", @"") forState:(UIControlStateNormal)];
    
    [self.cqr_mainView.cqr_contentTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.cqr_mainView.cqr_createBtn addTarget:self action:@selector(cqr_createBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    self.cqr_mainView.cqr_createBtn.tag = 2309;
    [[OpenShowView shareInstance] showDeledate:self showView:self.cqr_mainView];
}

-(void)scanLocalQRImageBtnAction:(UIButton *)sender {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = weakSelf;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [weakSelf presentViewController:picker animated:YES completion:nil];
    });
}

-(void)textFieldDidChange:(UITextField *)textField {
    self.cqr_mainView.cqr_createBtn.tag = 2309;
    [self.cqr_mainView.cqr_createBtn setTitle:NSLocalizedString(@"立即生成", @"") forState:(UIControlStateNormal)];
}

-(void)cqr_createBtnAction:(UIButton *)sender {
    if(sender.tag == 2309) {
        if(self.cqr_mainView.cqr_contentTextField.text.length > 0 && self.cqr_mainView.cqr_contentTextField.text.length < 500) {
            self.qr_image = [PS_GeneralTool createNonInterpolatedUIImageFormString:self.cqr_mainView.cqr_contentTextField.text withSize:self.cqr_mainView.cqr_contentTextField.text.length*20];
            self.cqr_mainView.cqr_imageView.image = self.qr_image;
            sender.tag = 2308;
            [sender setTitle:NSLocalizedString(@"存储到相册", @"") forState:(UIControlStateNormal)];
        } else {
            [[OpenShowView shareInstance] hide];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"内容长度限制为0-500，请重新输入", @"") preferredStyle:(UIAlertControllerStyleAlert)];
            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"以后再说", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [self presentViewController:alertController animated:YES completion:nil];

        }
    } else {
        // 保存二维码到相册
        UIImageWriteToSavedPhotosAlbum(self.qr_image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
}

#pragma mark -- <保存到相册>
-(void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    if(error){
        [[OpenShowView shareInstance] hide];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"保存图片失败", @"") preferredStyle:(UIAlertControllerStyleAlert)];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        [[OpenShowView shareInstance] hide];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"保存图片成功", @"") preferredStyle:(UIAlertControllerStyleAlert)];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)didHideOpenShowView {
    
}

-(void)finishScanWithContent:(NSString *)scanContent {
    if(scanContent && self.scanContentLabel) {
        [self refreshScanContentLabelText:scanContent];
        if(([scanContent rangeOfString:@"http://"].length > 0) ||
           ([scanContent rangeOfString:@"https://"].length > 0)) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"扫码内容已复制！\n是否确定打开该链接！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                [[UIPasteboard generalPasteboard] setString:scanContent];
                if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.scanContentString]]) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.scanContentString] options:@{} completionHandler:^(BOOL success) {
                        
                    }];
                }
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"取消", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {}]];
            [self presentViewController:alertController animated:YES completion:^{}];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"扫码内容已复制！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {}]];
            [self presentViewController:alertController animated:YES completion:^{}];
        }
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"未检测到二维码！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {}]];
        [self presentViewController:alertController animated:YES completion:^{}];
    }
}

-(void)refreshScanContentLabelText:(NSString *)text {
    self.scanContentString = text;
    [self.scanContentLabel setText:[NSString stringWithFormat:@"  %@  ", text] automaticWritingAnimationWithDuration:0.03 blinkingMode:(UILabelAWBlinkingModeUntilFinish)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UILabel *)scanContentLabel {
    if(!_scanContentLabel) {
        _scanContentLabel = [[UILabel alloc] init];
        _scanContentLabel.numberOfLines = 1;
        _scanContentLabel.font = [UIFont systemFontOfSize:15];
        _scanContentLabel.textColor = [UIColor colorWithRed:138./255. green:138./255. blue:138./255. alpha:1.];
        _scanContentLabel.textAlignment = NSTextAlignmentCenter;
        _scanContentLabel.clipsToBounds = YES;
        _scanContentLabel.backgroundColor = [UIColor colorWithRed:242./255. green:242./255. blue:242./255. alpha:1.];
        _scanContentLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapContentLabel:)];
        [_scanContentLabel addGestureRecognizer:tap];
        [self.view addSubview:_scanContentLabel];
    }
    return _scanContentLabel;
}

-(void)tapContentLabel:(UITapGestureRecognizer *)tap {
    NSString *msg = NSLocalizedString(@"暂无内容", @"");
    if(self.scanContentString.length > 0) {
        if([self.scanContentString isEqual:NSLocalizedString(@"点击下方按钮", @"")]) {
            msg = NSLocalizedString(@"暂无内容", @"");
        } else {
            msg = self.scanContentString;
        }
    }
    __weak typeof(self) weakSelf = self;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"扫描内容", @"") message:msg preferredStyle:(UIAlertControllerStyleAlert)];
    if(![msg isEqual:NSLocalizedString(@"暂无内容", @"")]) {
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"拷贝内容", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [[UIPasteboard generalPasteboard] setString:msg];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"拷贝成功！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
                    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                        
                    }]];
                    [strongSelf presentViewController:alertController animated:YES completion:nil];
                });
            });
        }]];
    }
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(UIButton *)scanBtn {
    if(!_scanBtn) {
        _scanBtn = [[UIButton alloc] init];
        _scanBtn.backgroundColor = [UIColor colorWithRed:242./255. green:242./255. blue:242./255. alpha:1.];
        _scanBtn.clipsToBounds = YES;
        [_scanBtn setTitleColor:[UIColor colorWithRed:138./255. green:138./255. blue:138./255. alpha:1.] forState:UIControlStateNormal];
        [_scanBtn addTarget:self action:@selector(scanBtnClickAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_scanBtn];
    }
    return _scanBtn;
}

-(UIButton *)scanLocalQRImageBtn {
    if(!_scanLocalQRImageBtn) {
        _scanLocalQRImageBtn = [[UIButton alloc] init];
        _scanLocalQRImageBtn.backgroundColor = [UIColor colorWithRed:242./255. green:242./255. blue:242./255. alpha:1.];
        _scanLocalQRImageBtn.clipsToBounds = YES;
        [_scanLocalQRImageBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_scanLocalQRImageBtn setTitleColor:[UIColor colorWithRed:138./255. green:138./255. blue:138./255. alpha:1.] forState:(UIControlStateNormal)];
        [_scanLocalQRImageBtn setTitle:NSLocalizedString(@"扫描图片", @"") forState:(UIControlStateNormal)];
        [_scanLocalQRImageBtn addTarget:self action:@selector(scanLocalQRImageBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:_scanLocalQRImageBtn];
    }
    return _scanLocalQRImageBtn;
}

-(UIButton *)createQRImageBtn {
    if(!_createQRImageBtn) {
        _createQRImageBtn = [[UIButton alloc] init];
        _createQRImageBtn.backgroundColor = [UIColor colorWithRed:242./255. green:242./255. blue:242./255. alpha:1.];
        _createQRImageBtn.clipsToBounds = YES;
        [_createQRImageBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_createQRImageBtn setTitleColor:[UIColor colorWithRed:138./255. green:138./255. blue:138./255. alpha:1.] forState:(UIControlStateNormal)];
        [_createQRImageBtn setTitle:NSLocalizedString(@"生成二维码", @"") forState:(UIControlStateNormal)];
        [_createQRImageBtn addTarget:self action:@selector(createQRImageBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:_createQRImageBtn];
    }
    return _createQRImageBtn;
}

@end
