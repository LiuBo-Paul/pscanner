//
//  AppDelegate.h
//  PScanner
//
//  Created by LiuBo on 2019/7/25.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

