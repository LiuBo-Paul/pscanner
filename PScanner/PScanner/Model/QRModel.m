//
//  QRModel.m
//  ScanDemo
//
//  Created by Paul on 13/03/2017.
//  Copyright © 2017 Paul. All rights reserved.
//

#import "QRModel.h"
#import "QRSourceHelper.h"
#import "MD5Encrypt.h"

@interface QRModel()<NSCopying>

@end

@implementation QRModel

-(id)initWithDic:(NSDictionary *)dic {
    if(self) {
        NSString *identifier = [NSString stringWithFormat:@"%@_%u", [[QRSourceHelper sharedService] getCurrentDateString], 100000 + (arc4random() % 999999)];
        identifier = [MD5Encrypt MD5ForLower16Bate:identifier];
        self.identifier = [NSString stringWithFormat:@"%@",identifier];
        self.title = [dic objectForKey:@"title"]?[dic objectForKey:@"title"]:[NSString stringWithFormat:@"%@", identifier];
        self.detail = [dic objectForKey:@"detail"]?[dic objectForKey:@"detail"]:@"";
        self.remark = [dic objectForKey:@"remark"]?[dic objectForKey:@"remark"]:@"";
        self.createTime = [dic objectForKey:@"createTime"]?[dic objectForKey:@"createTime"]:[NSDate date];
        self.changeTime = [dic objectForKey:@"changeTime"]?[dic objectForKey:@"changeTime"]:[NSDate date];
        self.isMark = [dic objectForKey:@"isMark"]?@"1":@"0";
        self.imageData = [dic objectForKey:@"imageData"]?[dic objectForKey:@"imageData"]:[self createImageDataWithAWord:[dic objectForKey:@"detail"]?[dic objectForKey:@"detail"]:@"P"];
    }
    return self;
}

-(id)copyWithZone:(NSZone *)zone {
    QRModel *model = [QRModel new];
    if(self) {
        model.identifier = self.identifier;
        model.title = self.title;
        model.detail = self.detail;
        model.remark = self.remark;
        model.createTime = self.createTime;
        model.changeTime = self.changeTime;
        model.isMark = self.isMark;
        model.imageData = self.imageData;
        return model;
    } else {
        model.identifier = @"0";
        model.title = @"";
        model.detail = @"";
        model.remark = @"";
        model.createTime = [NSDate date];
        model.changeTime = [NSDate date];
        model.isMark = @"0";
        model.imageData = [NSData data];
        return model;
    }
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.identifier forKey:@"identifier"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.detail forKey:@"detail"];
    [aCoder encodeObject:self.remark forKey:@"remark"];
    [aCoder encodeObject:self.createTime forKey:@"createTime"];
    [aCoder encodeObject:self.changeTime forKey:@"changeTime"];
    [aCoder encodeObject:self.isMark forKey:@"isMark"];
    [aCoder encodeObject:self.imageData forKey:@"imageData"];
}

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.identifier = [aDecoder decodeObjectForKey:@"identifier"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.detail = [aDecoder decodeObjectForKey:@"detail"];
        self.remark = [aDecoder decodeObjectForKey:@"remark"];
        self.createTime = [aDecoder decodeObjectForKey:@"createTime"];
        self.changeTime = [aDecoder decodeObjectForKey:@"changeTime"];
        self.isMark = [aDecoder decodeObjectForKey:@"isMark"];
        self.imageData = [aDecoder decodeObjectForKey:@"imageData"];
    }
    return self;
}

-(NSData *)createImageDataWithAWord:(NSString *)word {
    if(word.length == 0) {
        return [NSData data];
    } else if([word rangeOfString:@"http"].length >0) {
        word = [[word componentsSeparatedByString:@"://"] lastObject];
        word = [word substringWithRange:NSMakeRange(0, 1)];
    } else {
        word = [word substringWithRange:NSMakeRange(0, 1)];
    }
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    label.backgroundColor = [UIColor colorWithRed:138./255. green:138./255. blue:138./255. alpha:1.];
    label.text = word;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:15];
    NSData *data = UIImagePNGRepresentation([self snapshotImageWithView:label]);
    if(!data) {
        data = [NSData data];
    }
    return data;
}

- (UIImage *)snapshotImageWithView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.frame.size, YES, [UIScreen mainScreen].scale);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

@end
