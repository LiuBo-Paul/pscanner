//
//  QRModel.h
//  ScanDemo
//
//  Created by Paul on 13/03/2017.
//  Copyright © 2017 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface QRModel : NSObject

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *detail;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, strong) NSDate *createTime;
@property (nonatomic, strong) NSDate *changeTime;
@property (nonatomic, copy) NSString *isMark;
@property (nonatomic, strong) NSData *imageData;

-(id)initWithDic:(NSDictionary *)dic;
-(NSData *)createImageDataWithAWord:(NSString *)word;

@end
