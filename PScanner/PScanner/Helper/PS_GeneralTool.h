//
//  PS_GeneralTool.h
//  PScanner
//
//  Created by LiuBo on 2019/8/5.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PS_GeneralTool : NSObject

+(instancetype)sharedInstance;

+ (UIImage *)createNonInterpolatedUIImageFormString:(NSString *)string withSize:(CGFloat)size;

@end

NS_ASSUME_NONNULL_END
