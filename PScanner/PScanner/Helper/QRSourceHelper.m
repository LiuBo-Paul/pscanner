//
//  QRSourceHelper.m
//  IdealMobileOffice
//
//  Created by Paul on 13/03/2017.
//  Copyright © 2017 Paul. All rights reserved.
//

#import "QRSourceHelper.h"
#import "FSCacheManager.h"
#import "QRModel.h"

@implementation QRSourceHelper

static QRSourceHelper *sharedService = nil;
static NSString *FormatString = @"YYYY/MM/dd hh:mm:ss";

+(QRSourceHelper *)sharedService
{
    if (!sharedService)
    {
        sharedService = [[QRSourceHelper alloc]init];
    }
    return sharedService;
}

-(NSString *)getCurrentDateString {
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:FormatString];
    return [formatter stringFromDate:date];
}

-(NSString *)getTimeStrWithDate:(NSDate *)date {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:FormatString];
    return [formatter stringFromDate:date];
}

//将扫码结果存入数据库
-(void)saveScanString:(NSString *)stringValue {
    [[FSCacheManager sharedManager] searchDataWithName:HistoryFileName cacheType:(FSCacheTypeGeneral) successBlock:^(NSString *message, NSData *data, NSString *path) {
        if(data) {
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
            if(arr && arr.count>=0) {
                for (int i = 0; i < arr.count; i++) {
                    QRModel *oldModel = arr[i];
                    if([oldModel.detail isEqual:stringValue]) {
                        [arr removeObjectAtIndex:i];
                        break;
                    }
                }
                QRModel *model = [[QRModel alloc] initWithDic:@{@"detail":stringValue}];
                [arr insertObject:model atIndex:0];
                [[FSCacheManager sharedManager] saveData:[NSKeyedArchiver archivedDataWithRootObject:arr] name:HistoryFileName cacheType:(FSCacheTypeGeneral) successBlock:^(NSString *message, NSData *data, NSString *path) {
                    
                } failBlock:^(NSString *message) {
                    
                }];
            }
        }
    } failBlock:^(NSString *message) {
        QRModel *model = [[QRModel alloc] initWithDic:@{@"detail":stringValue}];
        [[FSCacheManager sharedManager] saveData:[NSKeyedArchiver archivedDataWithRootObject:@[model]] name:HistoryFileName cacheType:(FSCacheTypeGeneral) successBlock:^(NSString *message, NSData *data, NSString *path) {
            
        } failBlock:^(NSString *message) {
            
        }];
    }];
}

@end
