//
//  PS_GeneralTool.m
//  PScanner
//
//  Created by LiuBo on 2019/8/5.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "PS_GeneralTool.h"

@implementation PS_GeneralTool

static PS_GeneralTool *tool = nil;
+(instancetype)sharedInstance {
    if(tool == nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            tool = [[PS_GeneralTool alloc] init];
        });
    }
    return tool;
}

+ (UIImage *)createNonInterpolatedUIImageFormString:(NSString *)string withSize:(CGFloat)size {
    @autoreleasepool {
        //创建过滤器
        CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
        //过滤器恢复默认
        [filter setDefaults];
        //将NSString格式转化成NSData格式
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        [filter setValue:data forKeyPath:@"inputMessage"];
        //获取二维码过滤器生成的二维码
        CIImage *image = [filter outputImage];    
        CGRect extent = CGRectIntegral(image.extent);
        CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
        // 1.创建bitmap;
        size_t width = CGRectGetWidth(extent) * scale;
        size_t height = CGRectGetHeight(extent) * scale;
        CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, CGColorSpaceCreateDeviceGray(), (CGBitmapInfo)kCGImageAlphaNone);
        CIContext *context = [CIContext contextWithOptions:nil];
        CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
        CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
        CGContextScaleCTM(bitmapRef, scale, scale);
        CGContextDrawImage(bitmapRef, extent, bitmapImage);
        // 2.保存bitmap到图片
        CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
        CGContextRelease(bitmapRef);
        CGImageRelease(bitmapImage);
        return [UIImage imageWithCGImage:scaledImage];
    }
}

@end
