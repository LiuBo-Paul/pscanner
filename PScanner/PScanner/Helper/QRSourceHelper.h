//
//  QRSourceHelper.h
//  IdealMobileOffice
//
//  Created by Paul on 13/03/2017.
//  Copyright © 2017 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface QRSourceHelper : NSObject

/**
 帮助工具单例

 @return 单例
 */
+(QRSourceHelper *)sharedService;

/**
 获取当前时间

 @return 格式化后的时间字符串 YYYY/MM/dd hh:mm:ss
 */
-(NSString *)getCurrentDateString;

/**
 格式化date为时间字符串

 @param date date
 @return 格式化字符串
 */
-(NSString *)getTimeStrWithDate:(NSDate *)date;

/**
 将扫码内容存入缓存文件

 @param stringValue 扫码内容字符串
 */
-(void)saveScanString:(NSString *)stringValue;

@end
