//
//  HistoryListTableViewController.m
//  ScanDemo
//
//  Created by Paul on 13/03/2017.
//  Copyright © 2017 Paul. All rights reserved.
//

#import "HistoryListTableViewController.h"
#import "FSCacheManager.h"
#import "HistoryListTableViewCell.h"
#import "QRSourceHelper.h"
#import "OpenShowView.h"
#import "OpenShowMainView.h"

@interface HistoryListTableViewController ()<OpenShowViewDelegate>

@property (strong, nonatomic) NSMutableArray *dataArray;
@property (strong, nonatomic) UIPasteboard *pasteboard;
@property (nonatomic, strong) OpenShowMainView *editMainView;
@property (nonatomic, strong) OpenShowMainView *preWebView;

@end

@implementation HistoryListTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"扫描记录", @"");
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addNavItems];
    [self fetchHistoryData];
}

-(void)fetchHistoryData {
    __weak typeof(self) weakSelf = self;
    [[FSCacheManager sharedManager] searchDataWithName:HistoryFileName cacheType:(FSCacheTypeGeneral) successBlock:^(NSString *message, NSData *data, NSString *path) {
        if(data) {
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
            if(arr && arr.count>0) {
                for(int i = 0; i<arr.count; i++) {
                    QRModel *modelI = arr[i];
                    NSTimeInterval modelICreateTime = [modelI.createTime timeIntervalSinceNow];
                    for(int j = i+1; j<arr.count; j++){
                        QRModel *modelJ = arr[j];
                        NSTimeInterval modelJCreateTime = [modelJ.createTime timeIntervalSinceNow];
                        if(modelICreateTime <modelJCreateTime) {
                            [arr exchangeObjectAtIndex:i withObjectAtIndex:j];
                        }
                    }
                }
                weakSelf.dataArray = [arr mutableCopy];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                });
            } else {
                weakSelf.dataArray = [NSMutableArray new];
            }
        }
    } failBlock:^(NSString *message) {
        NSLog(@"%@", message);
    }];
}

-(void)addNavItems {
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithDisplayP3Red:138./255. green:138./255. blue:138./255. alpha:1.0];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:(UIBarButtonItemStyleDone) target:self action:@selector(backItemAction:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:138/255. green:138/255. blue:138/255. alpha:1.];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if(self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    } else {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
}

-(void)backItemAction:(UIBarButtonItem *)item {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSArray *views = [[tableView cellForRowAtIndexPath:indexPath].contentView subviews];
//    CGFloat maxHeight = 0;
//    for (int i = 0; i < views.count; i++) {
//        UIView *view = views[i];
//        if((view.frame.origin.y + view.frame.size.height) > maxHeight) {
//            maxHeight = view.frame.origin.y + view.frame.size.height;
//        }
//    }
//    return maxHeight + 10;
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"HistoryCell";
    HistoryListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"HistoryListTableViewCell" owner:self options:nil][0];
    }
    QRModel *model = self.dataArray[indexPath.row];
    if(model && model.title && model.detail) {
        cell.logoImageView.image = model.imageData?[UIImage imageWithData:model.imageData]:[UIImage imageNamed:@"scan"];
        cell.timeLabel.text = [NSString stringWithFormat:@"%@", [[QRSourceHelper sharedService] getTimeStrWithDate:model.createTime]];
        cell.nameLabel.text = [NSString stringWithFormat:@"%@", model.title];
        cell.detailLabel.text = [NSString stringWithFormat:@"%@", model.detail];
        cell.remarkLabel.text = [NSString stringWithFormat:@"%@", model.remark];
        [cell.favoriteBtn addTarget:self action:@selector(favoriteBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        cell.favoriteBtn.tag = 7900 + indexPath.row;
        if([model.detail rangeOfString:@"http"].length > 0) {
            cell.preViewBtn.hidden = NO;
            cell.preViewBtn.tag = 900 + indexPath.row;
            [cell.preViewBtn addTarget:self action:@selector(preViewBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        } else {
            cell.preViewBtn.hidden = YES;
        }
        if([[NSString stringWithFormat:@"%@", model.isMark] isEqual:@"1"]) {
            [cell.favoriteBtn setImage:[UIImage imageNamed:@"favorite"] forState:(UIControlStateNormal)];
        } else {
            [cell.favoriteBtn setImage:[UIImage imageNamed:@"unfavorite"] forState:(UIControlStateNormal)];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    } else {
        //nothing to do
    }
    return cell;
}

-(void)favoriteBtnAction:(UIButton *)sender {
    NSInteger index = sender.tag - 7900;
    if(index < self.dataArray.count) {
        QRModel *model = self.dataArray[index];
        if([[NSString stringWithFormat:@"%@", model.isMark] isEqual:@"1"]) {
            model.isMark = @"0";
        } else {
            model.isMark = @"1";
        }
        [self rewriteItemAtIndex:index newModel:model];
    }    
}

-(void)preViewBtnAction:(UIButton *)sender {
    NSInteger index = sender.tag - 900;
    if(index < self.dataArray.count) {
        QRModel *model = self.dataArray[index];
        self.preWebView = [[OpenShowMainView alloc] initWithPreViewFrame:CGRectMake(10, 80, UIScreen.mainScreen.bounds.size.width - 20, UIScreen.mainScreen.bounds.size.height - 160)];
        self.preWebView.clipsToBounds = YES;
        self.preWebView.layer.cornerRadius = 10;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.preWebView.mainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.detail]]];
        });
        [self.preWebView.closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [OpenShowView shareInstance].isAnima = YES;
        [[OpenShowView shareInstance] showDeledate:self showView:self.preWebView];
    }
}

-(void)closeBtnAction:(UIButton *)sender {
    if(self.preWebView.mainWebView.canGoBack) {
        [self.preWebView.mainWebView goBack];
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            __strong typeof(weakSelf) strongSelf = weakSelf;
            dispatch_async(dispatch_get_main_queue(), ^{                
                if([strongSelf.preWebView.mainWebView canGoBack]) {
                    [strongSelf.preWebView.closeBtn setImage:[UIImage imageNamed:@"back"] forState:(UIControlStateNormal)];
                } else {
                    [strongSelf.preWebView.closeBtn setImage:[UIImage imageNamed:@"close"] forState:(UIControlStateNormal)];
                }
            });
        });
    } else {
        [[OpenShowView shareInstance] hide];
    }
}

-(void)didHideOpenShowView {
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteItemAtIndex:indexPath.row];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    QRModel *model = self.dataArray[indexPath.row];
    if([[NSString stringWithFormat:@"%@", model.isMark] isEqual:@"0"]) {
        model.isMark = @"1";
    } else {
        model.isMark = @"0";
    }
    [self rewriteItemAtIndex:indexPath.row newModel:model];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.pasteboard = [UIPasteboard generalPasteboard];
    QRModel *model = self.dataArray[indexPath.row];
    self.pasteboard.string = model.detail;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"扫码内容已复制！\n是否确定打开该链接！", @"") preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.pasteboard.string]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.pasteboard.string] options:@{} completionHandler:^(BOOL success) {
                
            }];
        }
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"编辑", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // 编辑
        [self editMsgWithIndex:indexPath.row];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"取消", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        //nothing to do
    }]];
    [self presentViewController:alert animated:YES completion:^{}];
}

static CGFloat editViewHeight = 350;
-(void)editMsgWithIndex:(NSInteger)index {
    QRModel *model = self.dataArray[index];
    self.editMainView = [[OpenShowMainView alloc] initWithEditFrame:CGRectMake(20, (UIScreen.mainScreen.bounds.size.height - editViewHeight)/4.0, UIScreen.mainScreen.bounds.size.width - 40, editViewHeight)];
    [self.editMainView.cancelBtn addTarget:self action:@selector(cancelBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.editMainView.sureBtn addTarget:self action:@selector(sureBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.editMainView.favoriteBtn addTarget:self action:@selector(editFavoriteBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    self.editMainView.clipsToBounds = YES;
    self.editMainView.layer.cornerRadius = 10;
    if([[NSString stringWithFormat:@"%@", model.isMark] isEqual:@"1"]) {
        self.editMainView.favoriteBtn.tag = 9870;
        [self.editMainView.favoriteBtn setImage:[UIImage imageNamed:@"favorite"] forState:(UIControlStateNormal)];
    } else {
        self.editMainView.favoriteBtn.tag = 9871;
        [self.editMainView.favoriteBtn setImage:[UIImage imageNamed:@"unfavorite"] forState:(UIControlStateNormal)];
    }
    self.editMainView.sureBtn.tag = 450 + index;
    self.editMainView.msgTitleLabel.text = NSLocalizedString(@"标题", @"");
    self.editMainView.msgDetailLabel.text = NSLocalizedString(@"详情", @"");
    self.editMainView.msgRemarkLabel.text = NSLocalizedString(@"备注", @"");
    self.editMainView.titleLabel.text = NSLocalizedString(@"编辑", @"");
    self.editMainView.titleTextField.text = model.title;
    self.editMainView.titleTextField.placeholder = NSLocalizedString(@"请输入标题", @"");
    self.editMainView.detailTextField.text = model.detail;
    self.editMainView.detailTextField.placeholder = NSLocalizedString(@"请输入扫描详情", @"");
    self.editMainView.remarkTextField.text = model.remark;
    self.editMainView.remarkTextField.placeholder = NSLocalizedString(@"请输入扫描备注", @"");;
    [[OpenShowView shareInstance] showDeledate:self showView:self.editMainView];
}

-(void)cancelBtnAction:(UIButton *)sender {
    [[OpenShowView shareInstance] hide];
}

-(void)sureBtnAction:(UIButton *)sender {
    [[OpenShowView shareInstance] hide];
    NSInteger index = sender.tag - 450;
    if(index < self.dataArray.count) {
        QRModel *model = self.dataArray[index];
        model.title = self.editMainView.titleTextField.text;
        model.detail = self.editMainView.detailTextField.text;
        model.remark = self.editMainView.remarkTextField.text;
        model.isMark = self.editMainView.favoriteBtn.tag == 9870?@"1":@"0";
        [self rewriteItemAtIndex:index newModel:model];
    }
}

-(void)editFavoriteBtnAction:(UIButton *)sender {
    if(sender.tag == 9871) {
        self.editMainView.favoriteBtn.tag = 9870;
        [self.editMainView.favoriteBtn setImage:[UIImage imageNamed:@"favorite"] forState:(UIControlStateNormal)];
    } else {
        self.editMainView.favoriteBtn.tag = 9871;
        [self.editMainView.favoriteBtn setImage:[UIImage imageNamed:@"unfavorite"] forState:(UIControlStateNormal)];
    }
}

// 删除某行数据 并修改缓存文件
-(void)deleteItemAtIndex:(NSInteger)index {
    [self.dataArray removeObjectAtIndex:index];
    [self refreshTableViewWithRow:index];
    [[FSCacheManager sharedManager] saveData:[NSKeyedArchiver archivedDataWithRootObject:self.dataArray] name:HistoryFileName cacheType:(FSCacheTypeGeneral) successBlock:^(NSString *message, NSData *data, NSString *path) {
        
    } failBlock:^(NSString *message) {
        
    }];
}

// 修改某行数据 并修改缓存文件
-(void)rewriteItemAtIndex:(NSInteger)index newModel:(QRModel *)newModel {
    [self.dataArray replaceObjectAtIndex:index withObject:newModel];
    [self refreshTableViewWithRow:index];
    [[FSCacheManager sharedManager] saveData:[NSKeyedArchiver archivedDataWithRootObject:[self.dataArray copy]] name:HistoryFileName cacheType:(FSCacheTypeGeneral) successBlock:^(NSString *message, NSData *data, NSString *path) {
        
    } failBlock:^(NSString *message) {
        
    }];
    [self refreshTableViewWithRow:index];
}

// 刷新列表某行
-(void)refreshTableViewWithRow:(NSInteger)row {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.tableView reloadData];
    });
}

-(NSMutableArray *)dataArray {
    if(!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

@end
