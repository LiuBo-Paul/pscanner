//
//  AboutUsViewController.m
//  PScanner
//
//  Created by LiuBo on 2019/8/5.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "AboutUsViewController.h"

static NSString *kAppID = @"1474456587";
static NSString *phoneNumber = @"13621905107";
static NSString *emailNumber = @"13621905107@163.com";

@interface AboutUsViewController ()

@property (strong, nonatomic) IBOutlet UILabel *contactLabel;
@property (strong, nonatomic) IBOutlet UILabel *feedbackLabel;
@property (strong, nonatomic) IBOutlet UILabel *commandLabel;
@property (strong, nonatomic) IBOutlet UILabel *moreApplicationLabel;

@property (strong, nonatomic) IBOutlet UIImageView *contactImageView;
@property (strong, nonatomic) IBOutlet UIImageView *feedbackImageView;
@property (strong, nonatomic) IBOutlet UIImageView *commandIamgeView;
@property (strong, nonatomic) IBOutlet UIImageView *moreApplicationImageView;

@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"设置中心", @"");
    self.contactLabel.text = NSLocalizedString(@"联系作者", @"");
    self.feedbackLabel.text = NSLocalizedString(@"问题反馈", @"");
    self.commandLabel.text = NSLocalizedString(@"顺手点赞", @"");
    self.moreApplicationLabel.text = NSLocalizedString(@"更多应用", @"");
    
    self.contactImageView.image = [UIImage imageWithData:[self createImageDataWithAWord:@"S"]];
    self.feedbackImageView.image = [UIImage imageWithData:[self createImageDataWithAWord:@"C"]];
    self.commandIamgeView.image = [UIImage imageWithData:[self createImageDataWithAWord:@"A"]];
    self.moreApplicationImageView.image = [UIImage imageWithData:[self createImageDataWithAWord:@"N"]];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:(UIBarButtonItemStyleDone) target:self action:@selector(backAction:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:138./255. green:138./255. blue:138./255. alpha:1.];
}

-(void)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)logoBtnAction:(UIButton *)sender {
    NSString *urlStr = [NSString stringWithFormat:@"https://itunes.apple.com/cn/app/id%@", kAppID];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr] options:@{} completionHandler:^(BOOL success) {
        
    }];
}

- (IBAction)contactMeBtnAction:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"联系方式", @"") message:@"" preferredStyle:(UIAlertControllerStyleAlert)];
    __weak typeof(self) weakSelf = self;
    [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@（%@）", NSLocalizedString(@"拨打电话", @""), phoneNumber] style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        dispatch_async(dispatch_get_main_queue(), ^{
            UIWebView *webView = [[UIWebView alloc] init];
            [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneNumber]]]];
            [strongSelf.view addSubview:webView];
        });
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"拷贝电话号码", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        [[UIPasteboard generalPasteboard] setString:phoneNumber];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"拷贝成功", @"") preferredStyle:(UIAlertControllerStyleAlert)];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@（%@）",NSLocalizedString(@"发送反馈邮件", @""), emailNumber] style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf sendEmailWithEmailNum:emailNumber title:[NSString stringWithFormat:@"%@\"%@\"",NSLocalizedString(@"反馈", @""), NSLocalizedString(@"AppName", @"")] content:[NSString stringWithFormat:@"%@：\n", NSLocalizedString(@"反馈内容", @"")]];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"拷贝邮箱", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        [[UIPasteboard generalPasteboard] setString:emailNumber];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"拷贝成功", @"") preferredStyle:(UIAlertControllerStyleAlert)];
        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"不要点这个", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", kAppID]] options:@{} completionHandler:^(BOOL success) {
            
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"取消", @"") style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alert animated:YES completion:^{
        
    }];
}

- (IBAction)feedbackBtnAction:(id)sender {
    NSString *itunesurl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/cn/app/id%@?mt=8&action=write-review", kAppID];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:itunesurl] options:@{} completionHandler:nil];
}

- (IBAction)commandBtnAction:(UIButton *)sender {
    NSString *itunesurl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/cn/app/id%@?mt=8&action=write-review", kAppID];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:itunesurl] options:@{} completionHandler:nil];
}

- (IBAction)moreApplicationBtnAction:(UIButton *)sender {
    NSString *itunesurl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/cn/app/id%@?mt=8", kAppID];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:itunesurl] options:@{} completionHandler:nil];
}

-(void)sendEmailWithEmailNum:(NSString *)emailNum title:(NSString *)title content:(NSString *)content {
    //创建可变的地址字符串对象
    NSMutableString *mailUrl = [[NSMutableString alloc] init];
    [mailUrl appendFormat:@"mailto:%@?", emailNum];
    //添加邮件主题
    [mailUrl appendFormat:@"&subject=%@", title];
    //添加邮件内容
    [mailUrl appendString:[NSString stringWithFormat:@"&body=%@", content]];
    //跳转到系统邮件App发送邮件
    NSString *emailPath = [mailUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:emailPath] options:@{} completionHandler:nil];
}

-(NSData *)createImageDataWithAWord:(NSString *)word {
    if(word.length == 0) {
        return [NSData data];
    } else if([word rangeOfString:@"http"].length >0) {
        word = [[word componentsSeparatedByString:@"://"] lastObject];
        word = [word substringWithRange:NSMakeRange(0, 1)];
    } else {
        word = [word substringWithRange:NSMakeRange(0, 1)];
    }
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    label.backgroundColor = [UIColor colorWithRed:138./255. green:138./255. blue:138./255. alpha:1.];
    label.text = word;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:20];
    NSData *data = UIImagePNGRepresentation([self snapshotImageWithView:label]);
    if(!data) {
        data = [NSData data];
    }
    return data;
}

- (UIImage *)snapshotImageWithView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.frame.size, YES, [UIScreen mainScreen].scale);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

@end
