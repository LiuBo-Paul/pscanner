//
//  QRViewController.m
//  QRWeiXinDemo
//
//  Created by Paul on 13/03/2017.
//  Copyright © 2017 Paul. All rights reserved.
//

#import "QRViewController.h"
#import "QRSourceHelper.h"
#import "QRView.h"
#import "HistoryListTableViewController.h"
#import "QRModel.h"
#import "FSCacheManager.h"

@interface QRViewController ()<AVCaptureMetadataOutputObjectsDelegate,QRViewDelegate>

@property (strong, nonatomic) AVCaptureDevice * device;
@property (strong, nonatomic) AVCaptureDeviceInput * input;
@property (strong, nonatomic) AVCaptureMetadataOutput * output;
@property (strong, nonatomic) AVCaptureSession * session;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer * preview;
@property (strong, nonatomic) QRView *qrRectView;
@property (strong, nonatomic) UIButton *lightBtn;

@end

@implementation QRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self checkAuthor];
    self.navigationItem.title = NSLocalizedString(@"AppName", @"App名字");
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:(UIBarButtonItemStyleDone) target:self action:@selector(backItemAction:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:138/255. green:138/255. blue:138/255. alpha:1.];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"light_off"] style:(UIBarButtonItemStyleDone) target:self action:@selector(lightItemAction:)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithRed:138/255. green:138/255. blue:138/255. alpha:1.];
    self.navigationItem.rightBarButtonItem.tag = 1003;
}

-(void)lightItemAction:(UIBarButtonItem *)item {
    if(item.tag == 1003) {
        if([self turnOnLed]) {
            [item setImage:[UIImage imageNamed:@"light_on"]];
            item.tag = 1004;
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"该设备无闪光灯！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [self presentViewController:alertController animated:YES completion:^{}];
        }
    } else {
        [item setImage:[UIImage imageNamed:@"light_off"]];
        [self turnOffLed];
        item.tag = 1003;
    }

}

-(void)backItemAction:(UIBarButtonItem *)item {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if(self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    } else {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
}

-(void)checkAuthor {
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    switch (authStatus) {
        case AVAuthorizationStatusNotDetermined: {
            [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                     dispatch_async(dispatch_get_main_queue(), ^{
                         if(granted) {
                             [self startScan];
                         } else {
                             NSString *tips = NSLocalizedString(@"你的权限受限!", @"");
                             [self.navigationController popViewControllerAnimated:YES];
                             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"打开相机需要授权！", @"") message:tips preferredStyle:(UIAlertControllerStyleAlert)];
                             [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                                 
                             }]];
                             [self presentViewController:alertController animated:YES completion:^{}];
                         }
                     });
                 }];
             }];
        } break;
        case AVAuthorizationStatusRestricted: {
            [self.navigationController popViewControllerAnimated:YES];
            NSString *tips = NSLocalizedString(@"你的权限受限!", @"");
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"打开相机需要授权！", @"") message:tips preferredStyle:(UIAlertControllerStyleAlert)];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [self presentViewController:alertController animated:YES completion:^{}];
        } break;
        case AVAuthorizationStatusDenied: {
            [self.navigationController popViewControllerAnimated:YES];
            NSString *tips = NSLocalizedString(@"需要保存图片到相册请授权本App可以访问相册\n设置方式:手机设置->隐私->照片允许本App访问相册", @"");
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"打开相机需要授权！", @"") message:tips preferredStyle:(UIAlertControllerStyleAlert)];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [self presentViewController:alertController animated:YES completion:^{}];
        } break;
        case AVAuthorizationStatusAuthorized: {
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                [self startScan];
            } else {
                [self.navigationController popViewControllerAnimated:YES];
                NSString *tips = NSLocalizedString(@"设备无摄像头", @"");
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:tips preferredStyle:(UIAlertControllerStyleAlert)];
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    
                }]];
                [self presentViewController:alertController animated:YES completion:^{}];
            }
        } break;
    }
}

-(void)startScan {
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    // Input
    _input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];
    // Output
    _output = [[AVCaptureMetadataOutput alloc]init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    // Session
    _session = [[AVCaptureSession alloc]init];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    if ([_session canAddInput:self.input]) {
        [_session addInput:self.input];
    }
    if ([_session canAddOutput:self.output]) {
        [_session addOutput:self.output];
    }
    // 条码类型 AVMetadataObjectTypeQRCode
    _output.metadataObjectTypes =@[AVMetadataObjectTypeQRCode];
    // Preview
    _preview =[AVCaptureVideoPreviewLayer layerWithSession:_session];
    _preview.videoGravity =AVLayerVideoGravityResize;
    _preview.frame =self.view.layer.bounds;
    [self.view.layer insertSublayer:_preview atIndex:0];
    [_session startRunning];

    CGRect screenRect = [UIScreen mainScreen].bounds;
    self.qrRectView = [[QRView alloc] initWithFrame:screenRect];
    self.qrRectView.transparentArea = CGSizeMake(300, 300);
    self.qrRectView.backgroundColor = [UIColor clearColor];
    self.qrRectView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    self.qrRectView.delegate = self;
    [self.view addSubview:self.qrRectView];
    
    //修正扫描区域
    CGFloat screenHeight = self.view.frame.size.height;
    CGFloat screenWidth = self.view.frame.size.width;
    CGRect cropRect = CGRectMake((screenWidth - self.qrRectView.transparentArea.width) / 2,
                                 (screenHeight - self.qrRectView.transparentArea.height) / 2,
                                 self.qrRectView.transparentArea.width,
                                 self.qrRectView.transparentArea.height);

    [_output setRectOfInterest:CGRectMake(cropRect.origin.y / screenHeight,
                                          cropRect.origin.x / screenWidth,
                                          cropRect.size.height / screenHeight,
                                          cropRect.size.width / screenWidth)];
}

-(BOOL)turnOffLed {
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([device hasTorch]) {
        [device lockForConfiguration:nil];
        [device setTorchMode: AVCaptureTorchModeOff];
        [device unlockForConfiguration];
        return YES;
    } else {
        return NO;
    }
}

-(BOOL)turnOnLed {
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([device hasTorch]) {
        [device lockForConfiguration:nil];
        [device setTorchMode: AVCaptureTorchModeOn];
        [device unlockForConfiguration];
        return YES;
    } else {
        return NO;
    }
}

#pragma mark QRViewDelegate
-(void)scanTypeConfig:(QRItem *)item {
    CGRect screenRect = [UIScreen mainScreen].bounds;
    if (item.type == QRItemTypeQRCode) {
        [self.qrRectView removeFromSuperview];
        self.qrRectView = [[QRView alloc] initWithFrame:screenRect];
        self.qrRectView.transparentArea = CGSizeMake(300, 300);
        self.qrRectView.backgroundColor = [UIColor clearColor];
        self.qrRectView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        self.qrRectView.delegate = self;
        [self.view addSubview:self.qrRectView];
        _output.metadataObjectTypes =@[AVMetadataObjectTypeQRCode];
    }  else if (item.type == QRItemTypeOther) {
        [self.qrRectView removeFromSuperview];
        self.qrRectView = [[QRView alloc] initWithFrame:screenRect];
        self.qrRectView.transparentArea = CGSizeMake(300, 100);
        self.qrRectView.backgroundColor = [UIColor clearColor];
        self.qrRectView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        self.qrRectView.delegate = self;
        [self.view addSubview:self.qrRectView];
        _output.metadataObjectTypes = @[AVMetadataObjectTypeEAN13Code,
                                        AVMetadataObjectTypeEAN8Code,
                                        AVMetadataObjectTypeCode128Code,
                                        AVMetadataObjectTypeQRCode];
    }
    [self.view bringSubviewToFront:self.lightBtn];
}

#pragma mark AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    NSString *stringValue;
    if ([metadataObjects count] >0) {
        //stop scan
        [_session stopRunning];
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex:0];
        stringValue = metadataObject.stringValue;
    }
    [self.navigationController popViewControllerAnimated:YES];
    if(self.delegate && [self.delegate respondsToSelector:@selector(finishScanWithContent:)]) {
        [[QRSourceHelper sharedService] saveScanString:stringValue];
        [self.delegate finishScanWithContent:stringValue];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
