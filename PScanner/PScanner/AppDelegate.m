//
//  AppDelegate.m
//  PScanner
//
//  Created by LiuBo on 2019/7/25.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CanScanPasteboard"];
    ViewController *viewController = [[ViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    self.window.rootViewController = nav;
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CanScanPasteboard"];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


/*
 FSCacheManager/FSCacheManager.m
 FSCacheManager/FSCacheManager.m:        failBlock(NSLocalizedString(@"文件写入失败", @""));
 FSCacheManager/FSCacheManager.m:        successBlock(NSLocalizedString(@"文件写入成功！", @""), data, fullPath);
 FSCacheManager/FSCacheManager.m:        failBlock(NSLocalizedString(@"文件读取失败，文件不存在", @""));
 FSCacheManager/FSCacheManager.m:            successBlock(NSLocalizedString(@"读取成功", @""), data, fullPath);
 FSCacheManager/FSCacheManager.m:            failBlock(NSLocalizedString(@"文件读取失败，内容为空", @""));
 Controller/AboutUsViewController.m
 Controller/AboutUsViewController.m:    self.navigationItem.title = NSLocalizedString(@"设置中心", @"");
 Controller/AboutUsViewController.m:    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"联系方式", @"") message:@"" preferredStyle:(UIAlertControllerStyleAlert)];
 Controller/AboutUsViewController.m:    [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@（%@）", NSLocalizedString(@"拨打电话", @""), phoneNumber] style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 Controller/AboutUsViewController.m:    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"拷贝电话号码", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 Controller/AboutUsViewController.m:        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"拷贝成功", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 Controller/AboutUsViewController.m:        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"拷贝成功", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 Controller/AboutUsViewController.m:        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 Controller/AboutUsViewController.m:    [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@（%@）",NSLocalizedString(@"发送反馈邮件", @""), emailNumber] style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 Controller/AboutUsViewController.m:        [weakSelf sendEmailWithEmailNum:emailNumber title:[NSString stringWithFormat:@"%@\"%@\"",NSLocalizedString(@"反馈", @""), NSLocalizedString(@"AppName", @"")] content:[NSString stringWithFormat:@"%@：\n", NSLocalizedString(@"反馈内容", @"")]];
 Controller/AboutUsViewController.m:        [weakSelf sendEmailWithEmailNum:emailNumber title:[NSString stringWithFormat:@"%@\"%@\"",NSLocalizedString(@"反馈", @""), NSLocalizedString(@"AppName", @"")] content:[NSString stringWithFormat:@"%@：\n", NSLocalizedString(@"反馈内容", @"")]];
 Controller/AboutUsViewController.m:    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"拷贝邮箱", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 Controller/AboutUsViewController.m:        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"拷贝成功", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 Controller/AboutUsViewController.m:        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"拷贝成功", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 Controller/AboutUsViewController.m:        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 Controller/AboutUsViewController.m:    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"不要点这个", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 Controller/AboutUsViewController.m:    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"取消", @"") style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
 Controller/HistoryListTableViewController.m
 Controller/HistoryListTableViewController.m:    self.navigationItem.title = NSLocalizedString(@"扫描记录", @"");
 Controller/HistoryListTableViewController.m:    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"扫码内容已复制！\n是否确定打开该链接！", @"") preferredStyle:UIAlertControllerStyleAlert];
 Controller/HistoryListTableViewController.m:    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"扫码内容已复制！\n是否确定打开该链接！", @"") preferredStyle:UIAlertControllerStyleAlert];
 Controller/HistoryListTableViewController.m:    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 Controller/HistoryListTableViewController.m:    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"编辑", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 Controller/HistoryListTableViewController.m:    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"取消", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
 Controller/HistoryListTableViewController.m:    self.editMainView.titleLabel.text = NSLocalizedString(@"编辑", @"");
 Controller/HistoryListTableViewController.m:    self.editMainView.titleTextField.placeholder = NSLocalizedString(@"请输入标题", @"");
 Controller/HistoryListTableViewController.m:    self.editMainView.detailTextField.placeholder = NSLocalizedString(@"请输入扫描详情", @"");
 Controller/HistoryListTableViewController.m:    self.editMainView.remarkTextField.placeholder = NSLocalizedString(@"请输入扫描备注", @"");;
 Controller/QRViewController.m
 Controller/QRViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"该设备无闪光灯！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 Controller/QRViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"该设备无闪光灯！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 Controller/QRViewController.m:            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 Controller/QRViewController.m:                             NSString *tips = NSLocalizedString(@"你的权限受限!", @"");
 Controller/QRViewController.m:                             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"打开相机需要授权！", @"") message:tips preferredStyle:(UIAlertControllerStyleAlert)];
 Controller/QRViewController.m:                             [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 Controller/QRViewController.m:            NSString *tips = NSLocalizedString(@"你的权限受限!", @"");
 Controller/QRViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"打开相机需要授权！", @"") message:tips preferredStyle:(UIAlertControllerStyleAlert)];
 Controller/QRViewController.m:            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 Controller/QRViewController.m:            NSString *tips = NSLocalizedString(@"需要保存图片到相册请授权本App可以访问相册\n设置方式:手机设置->隐私->照片允许本App访问相册", @"");
 Controller/QRViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"打开相机需要授权！", @"") message:tips preferredStyle:(UIAlertControllerStyleAlert)];
 Controller/QRViewController.m:            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 FFDropDownMenu/FFDropDownMenuCell.m
 FFDropDownMenu/FFDropDownMenuCell.m://        FFLog(@"您传入的图片为空图片,框架内部默认不做任何处理。若您的确不想传入图片，则请忽略此处打印");
 FFDropDownMenu/FFDropDownMenuView.h
 FFDropDownMenu/FFDropDownMenuView.h: *               若使用自定义的cell,就传自定义cell的类名,若cell是xib,则传@"类名.xib"
 View/OpenShowMainView.m
 View/OpenShowMainView.m:    [self.closeBtn setTitle:NSLocalizedString(@"页面加载中...", @"") forState:(UIControlStateNormal)];
 View/OpenShowMainView.m:            [weakSelf.closeBtn setTitle:NSLocalizedString(@"页面加载超时，请重新加载", @"") forState:(UIControlStateNormal)];
 View/OpenShowMainView.m:    [self.closeBtn setTitle:NSLocalizedString(@"加载完成", @"") forState:(UIControlStateNormal)];
 View/OpenShowMainView.m:    [self.closeBtn setTitle:NSLocalizedString(@"加载失败", @"") forState:(UIControlStateNormal)];
 View/QRMenu.m
 View/QRMenu.m:    } titile:NSLocalizedString(@"二维码扫描", @"")];
 View/QRMenu.m:    } titile:NSLocalizedString(@"条形码扫描", @"")];
 ViewController.m
 ViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"检测到复制了新内容，是否马上生成二维码？", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"检测到复制了新内容，是否马上生成二维码？", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"马上生成", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 ViewController.m:            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"以后再说", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 ViewController.m:            [weakSelf refreshScanContentLabelText:NSLocalizedString(@"点击下方按钮", @"")];
 ViewController.m:            [weakSelf.scanBtn setTitle:NSLocalizedString(@"开始扫描", @"") forState:UIControlStateNormal];
 ViewController.m:    FFDropDownMenuModel *menuModel0 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:NSLocalizedString(@"扫码记录", @"") menuItemIconName:@"scan"  menuBlock:^{
 ViewController.m:    FFDropDownMenuModel *menuModel2 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:NSLocalizedString(@"设置中心", @"") menuItemIconName:@"aboutus" menuBlock:^{
 ViewController.m:    [self.cqr_mainView.cqr_createBtn setTitle:NSLocalizedString(@"立即生成", @"") forState:(UIControlStateNormal)];
 ViewController.m:            [sender setTitle:NSLocalizedString(@"存储到相册", @"") forState:(UIControlStateNormal)];
 ViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"内容长度限制为0-500，请重新输入", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"内容长度限制为0-500，请重新输入", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"以后再说", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 ViewController.m:        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"保存图片失败", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"保存图片失败", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 ViewController.m:        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"保存图片成功", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"保存图片成功", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 ViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"扫码内容已复制！\n是否确定打开该链接！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"扫码内容已复制！\n是否确定打开该链接！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 ViewController.m:            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"取消", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {}]];
 ViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"扫码内容已复制！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"扫码内容已复制！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:            [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {}]];
 ViewController.m:        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"未检测到二维码！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"未检测到二维码！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {}]];
 ViewController.m:    NSString *msg = @"暂无内容";
 ViewController.m:        if([self.scanContentString isEqual:NSLocalizedString(@"点击下方按钮", @"")]) {
 ViewController.m:            msg = @"暂无内容";
 ViewController.m:    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"扫描内容", @"") message:msg preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:    if(![msg isEqual:@"暂无内容"]) {
 ViewController.m:        [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"拷贝内容", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 ViewController.m:                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"拷贝成功！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"拷贝成功！", @"") preferredStyle:(UIAlertControllerStyleAlert)];
 ViewController.m:                    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 ViewController.m:    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"确定", @"") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
 ViewController.m:        [_scanLocalQRImageBtn setTitle:NSLocalizedString(@"扫描图片", @"") forState:(UIControlStateNormal)];
 ViewController.m:        [_createQRImageBtn setTitle:NSLocalizedString(@"生成二维码", @"") forState:(UIControlStateNormal)];
 */
@end
